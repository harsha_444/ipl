class Embed{
 autoComplete(db, req){
   console.log(req.params);
  const x = req.query.name;
  return db.collection('matches').find({
    $or: [
      {
        "team1": {
          $regex: x + ".*"
        }
      }, {
        "team2": {
          $regex: x + ".*"
        }
      }
    ]
  }).toArray()

}

 scoreCard(db, req, y){
  const x = req.query.name;
  return db.collection('deliveries').aggregate([
    {
      $match: {
        match_id: parseInt(x)
      }
    }, {
      $sort: {
        inning: -1
      }
    }, {
      $group: {
        _id: "$batsman",
        total: {
          $sum: "$batsman_runs"
        },
        balls: {
          $sum: 1
        },
        inning: {
          $min: "$inning"
        }
      }
    }, {
      $sort: {
        inning: 1
      }
    }
  ]).skip((12 * (y - 1))).limit(13).toArray()
}

}

module.exports = Embed;
