const express = require('express');

const query = require('query-parser');

//import {Embed} from './embed.js';
//
const Embed = require('./embed.js');



var app = express();

const MongoClient = require('mongodb').MongoClient;

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
  res.send('index.html');

});

// class Embed{
//  autoComplete(db, req){
//   const x = req.query.name;
//   return db.collection('matches').find({
//     $or: [
//       {
//         "team1": {
//           $regex: x + ".*"
//         }
//       }, {
//         "team2": {
//           $regex: x + ".*"
//         }
//       }
//     ]
//   }).toArray()
//
// }
//
//  scoreCard(db, req, y){
//   const x = req.query.name;
//   return db.collection('deliveries').aggregate([
//     {
//       $match: {
//         match_id: parseInt(x)
//       }
//     }, {
//       $sort: {
//         inning: -1
//       }
//     }, {
//       $group: {
//         _id: "$batsman",
//         total: {
//           $sum: "$batsman_runs"
//         },
//         balls: {
//           $sum: 1
//         },
//         inning: {
//           $min: "$inning"
//         }
//       }
//     }, {
//       $sort: {
//         inning: 1
//       }
//     }
//   ]).skip((12 * (y - 1))).limit(13).toArray()
// }
//
// }


app.get('/auto', (req, res) => {
  //console.log(x);
  let obj1 = new Embed;
  MongoClient.connect('mongodb://localhost:27017/ipl', (err, db) => {
    if (err) {
      console.log('Unable to connect to server');
    }
    console.log('Connected to server');

    obj1.autoComplete(db, req).then((docs) => {

      res.json(docs)

      //console.log(JSON.stringify(docs,undefined,2));
    }, (err) => {
      console.log('Unable to fetch Todos', err);
    })

    //db.close();

  });

});

app.get('/delivery', (req, res) => {
  // var x = req.query.name;
  var y = parseInt(req.query.data);

  let obj2 = new Embed;
  //console.log(x)
  MongoClient.connect('mongodb://localhost:27017/ipl', (err, db) => {
    if (err) {
      console.log('Unable to connect to server');
    }
    console.log('Connected to server');

    obj2.scoreCard(db, req, y).then((docs) => {

      res.json(docs)

      //console.log(JSON.stringify(docs,undefined,2));
    }, (err) => {
      console.log('Unable to fetch Todos', err);
    })

    //db.close();

  });

});

app.listen(3000);
